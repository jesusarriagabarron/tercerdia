<?php

// validamos que la ruta no contenga ../ defensa contra directory traversal
if(!isset($_GET['img']) || substr_count($_GET['img'],'..') > 0){
    exit;
}	
	
$dominiosValidos = array(
					      'http://estadiototal.my.phpcloud.com',
					      'http://portadas.local',
                                              'http://www.tercerdia.com',
'http://tercerdia.com',
						  '/web'
					      );	
$imagenValida=0;
$imagen = $_GET['img'];

foreach ($dominiosValidos as $k => $v){
	if(substr_count($imagen,$v)>0)
		$imagenValida=1;
}

if(!$imagenValida)
	die(".");
$width = @$_GET['width'];
if(!is_numeric($width))
	$width="";

$height = @$_GET['height'];
if(!is_numeric($height))
	$height="";
	
$coordinates=@$_GET['coordinates'];
if(!preg_match("/^([1-9]|[1-9][0-9])" ."(,([1-9]|[1-9][0-9]))$/", $coordinates))
	$coordinates="50,50";


	
include_once("ImageManipulator.php");

/*       PARAMETROS CONFIGURACION             */

// path en donde se guardan las imagenes resampleadas
$imgResampledFolder = '/web/webhost/tvazteca/htdocs/imagenes/resapled';

// nombre de la imagen a regresar
$vector = explode(",", $coordinates);
$imgResampledPath = $imgResampledFolder.'/'.md5($imagen.'seed').'_'.$width.'_x_'.$height.'_focus_'.$vector[0]."_x_".$vector[1];

/*
if(file_exists($imgResampledPath)){// file_exists($imgResampledPath)
    $im = new ImageManipulator($imgResampledPath);
    $im->imprimir();
    exit;
}
*/

/* 		FIN PARAMS CONFIGURACION 			  */

$im = new ImageManipulator($imagen);


//Obtengo las dimensiones de la imagen
$imWidth = $im->getWidth();
$imHeight = $im->getHeight();

if(!$width||!$height)
{
	if($width){
		$resizeWidth = $width; 
		$proporcion = $resizeWidth*100/$imWidth;
		$resizeHeight = round($proporcion*$imHeight/100);
		$im->resample($resizeWidth, $resizeHeight);
	}
	else{
		if($height){
			$resizeHeight = $height; 
			$proporcion = $resizeHeight*100/$imHeight;
			$resizeWidth = round($proporcion*$imWidth/100);
			$im->resample($resizeWidth, $resizeHeight);
		}
	}
}
else{
/*       PARAMETROS CONFIGURACION             */

//Defino si el crop er horizontal o vertical
if($width>$height)
	$crophorizontal=1;
else
	$crophorizontal=0;

//Defino si la imagen es horizontal o vertical
if($imWidth>$imHeight)
	$imHorizontal=1;
else
	$imHorizontal=0;
	
if ($imHorizontal){
	//Imagen Horizontal
	if($crophorizontal){
		
		//Imagen Horizontal y Crop Horizontal
		//resize hasta el vertical
		
		$resizeWidth = $width; 
		$proporcion = $resizeWidth*100/$imWidth;
		$resizeHeight = round($proporcion*$imHeight/100);
		$im->resample($resizeWidth, $resizeHeight);

		$midwidth = round($width/2);
		$midheight = round($height/2);
		
		$vector = explode(",", $coordinates);
		
		$centreX = round(($vector[0]*$resizeWidth)/100);
		$centreY = round(($vector[1]*$resizeHeight)/100);
			
		$x1 = $centreX - $midwidth;
		$y1 = $centreY - $midheight;
		
		$x2 = $centreX + $midwidth;
		$y2 = $centreY + $midheight;
		
		if($resizeHeight>$height){
			//crop xq se sale la imagen
			if($y1<0){
				$y2=$y2-$y1;
				$y1=0;
			}
			else{
				if($y2>$resizeHeight){
					$y2=$resizeHeight;
					$y1=$resizeHeight-$height;
				}
			}
			$im->crop(0,$y1,$width,$y2);			
		}
		else{
			
			if($resizeHeight<$height){
				if($imHeight>=$height){
					
					//nuevo resize
					$resizeHeight = $height; 
					$proporcion = $resizeHeight*100/$imHeight;
					$resizeWidth = round($proporcion*$imWidth/100);
					$im = new ImageManipulator($imagen);
					$im->resample($resizeWidth, $resizeHeight);
					if($resizeWidth>$width){
						//crop
						$centreX = round(($vector[0]*$resizeWidth)/100);
						$centreY = round(($vector[1]*$resizeHeight)/100);
							
						$x1 = $centreX - $midwidth;
						$y1 = $centreY - $midheight;
						
						$x2 = $centreX + $midwidth;
						$y2 = $centreY + $midheight;
						
						if($x1<0){
							$x2=$x2-$x1;
							$x1=0;
						}
						else{
							if($x2>$resizeWidth){
								$x2=$resizeWidth;
								$x1=$resizeWidth-$width;
							}
						}
						$im->crop($x1,0,$x2,$height);
					}
					else{
						$im->enlargeCanvas($width, $height);
					}
				}
				else{
					$im->enlargeCanvas($width, $height);
				}
			}
		}
	}
	else{
		//Img horizontal crop vertical
		$resizeHeight = $height; 
		$proporcion = $resizeHeight*100/$imHeight;
		$resizeWidth = round($proporcion*$imWidth/100);
		$im->resample($resizeWidth, $resizeHeight);
		
		$midwidth = round($width/2);
		$midheight = round($height/2);
		
		$vector = explode(",", $coordinates);
		
		$centreX = round(($vector[0]*$resizeWidth)/100);
		$centreY = round(($vector[1]*$resizeHeight)/100);
			
		$x1 = $centreX - $midwidth;
		$y1 = $centreY - $midheight;
		
		$x2 = $centreX + $midwidth;
		$y2 = $centreY + $midheight;
		
		if($x1<0){
			//desde 0 hasta el max
			$x1=0;
			$x2=$width;
			$im->crop($x1, 0, $x2, $resizeHeight);
		}else{
			if($x2>$resizeWidth){
				$x1=$resizeWidth-$width;
				$x2=$resizeWidth;
				$im->crop($x1, 0, $x2, $resizeHeight);
				//desde el max menos el x 
				
			}
			else{
				//normal
				$im->crop($x1, 0, $x2, $resizeHeight); // takes care of out of boundary conditions automatically
			}
		}
	}
}
else{
	//Imagen Vertical
	if($crophorizontal){
		//Img vertical crop horizontal
		$resizeWidth = $width; 
		$proporcion = $resizeWidth*100/$imWidth;
		$resizeHeight = round($proporcion*$imHeight/100);
		$im->resample($resizeWidth, $resizeHeight);
		
		$midwidth = round($width/2);
		$midheight = round($height/2);
		
		$vector = explode(",", $coordinates);
		
		$centreX = round(($vector[0]*$resizeWidth)/100);
		$centreY = round(($vector[1]*$resizeHeight)/100);
			
		$x1 = $centreX - $midwidth;
		$y1 = $centreY - $midheight;
		
		$x2 = $centreX + $midwidth;
		$y2 = $centreY + $midheight;
		
		if($y1<0){
			//desde 0 hasta el max
			$y1=0;
			$y2=$height;
			$im->crop(0, $y1, $resizeWidth, $y2);
		}else{
			if($y2>$resizeHeight){
				$y1=$resizeHeight-$height;
				$y2=$resizeHeight;
				$im->crop(0, $y1, $resizeWidth, $y2);
				//desde el max menos el x 
				
			}
			else{
				//normal
				$im->crop(0, $y1, $resizeWidth, $y2); // takes care of out of boundary conditions automatically
			}
		}
		
	}
	else{
		//Imagen Vertical y Crop Vertical
		$resizeHeight = $height; 
		$proporcion = $resizeHeight*100/$imHeight;
		$resizeWidth = round($proporcion*$imWidth/100);
		$im->resample($resizeWidth, $resizeHeight);

		$midwidth = round($width/2);
		$midheight = round($height/2);
		
		$vector = explode(",", $coordinates);
		
		$centreX = round(($vector[0]*$resizeWidth)/100);
		$centreY = round(($vector[1]*$resizeHeight)/100);
			
		$x1 = $centreX - $midwidth;
		$y1 = $centreY - $midheight;
		
		$x2 = $centreX + $midwidth;
		$y2 = $centreY + $midheight;
		
		if($resizeWidth>$width){
			//crop xq se sale la imagen
			if($x1<0){
				$x2=$x2-$x1;
				$x1=0;
			}
			else{
				if($x2>$resizeWidth){
					$x2=$resizeWidth;
					$x1=$resizeWidth-$width;
				}
			}
			$im->crop($x1,0,$x2,$height);			
		}
		else{
			
			if($resizeWidth<$width){
				
				if($imWidth>=$width){
					//nuevo resize
					$resizeWidth = $width; 
					$proporcion = $resizeWidth*100/$imWidth;
					$resizeHeight = round($proporcion*$imHeight/100);
					$im = new ImageManipulator($imagen);
					$im->resample($resizeWidth, $resizeHeight);
					if($resizeHeight>$height){
						//crop
						$centreX = round(($vector[0]*$resizeWidth)/100);
						$centreY = round(($vector[1]*$resizeHeight)/100);
							
						$x1 = $centreX - $midwidth;
						$y1 = $centreY - $midheight;
						
						$x2 = $centreX + $midwidth;
						$y2 = $centreY + $midheight;
						
						if($y1<0){
							$y2=$y2-$y1;
							$y1=0;
						}
						else{
							if($y2>$resizeHeight){
								$y2=$resizeHeight;
								$y1=$resizeHeight-$height;
							}
						}
						$im->crop(0,$y1,$width,$y2);
					}
					else{
						$im->enlargeCanvas($width, $height);
					}
				}
				else{
					$im->enlargeCanvas($width, $height);
				}
			}
		}
	}
}
}
$im->imprimir();
//$im->save($imgResampledPath);

?>