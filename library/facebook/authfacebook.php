<?php
class facebook_authfacebook implements Zend_Auth_Adapter_Interface  {
	private $fb;

	public function __construct($fb){
  			$this->fb=$fb;
	}


	public function authenticate()  {
    	$user=$this->fb->getUser();
    	
    	if(!$user){
			return new Zend_Auth_Result(Zend_Auth_Result::FAILURE_CREDENTIAL_INVALID,
					false, array('Token was not set'));
		}
		$userinfo=$this->fb->api('/'.$user);
		if((int)$userinfo['id']==1112151402 || (int)$userinfo['id']==1053649936)
		$userinfo['admin']=1;	
		else
		$userinfo['admin']=0;
		
                                           
		return new Zend_Auth_Result(Zend_Auth_Result::SUCCESS,$userinfo);
	}
}