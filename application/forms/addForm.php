<?php

class Application_Form_addForm extends Zend_Form{

	public function init(){
		$this->setAttrib('enctype', 'multipart/form-data');
		$fc = Zend_Controller_Front::getInstance();
		$this->setMethod('post');
		$this->setAction($fc->getBaseUrl().'/admin/nuevo');
		$this->setAttrib('id','form1');
		
		$dbcategorias=new Application_Model_Categoria();
		$rowscategorias=$dbcategorias->listar_ordenados();

		
		$categorias=$this->createElement('multiCheckbox','categorias');
		$categorias->setLabel('Selecciona la(s) Categoria(s)')
		->setRequired(true)
		->addErrorMessage('Seleccione al menos 1 categoria');
		
		foreach($rowscategorias as $cat){
				$categorias->addMultiOption($cat['idCategoria'],($cat['cTitulo']));
		}
	
		$nombre_portada = $this->createElement('text','nombre_portada');
		$nombre_portada->setLabel('Nombre de la portada')
						->setRequired(true)
						->addValidator('NotEmpty')
						->addValidator('Db_NoRecordExists',false,
               					array('table' => 'TDBackground','field' => 'cTitulo')
               					);

		
		$nombre_portada->getValidator('NotEmpty')->setMessage('Este campo no puede estar vacío');
		$nombre_portada->getValidator('Db_NoRecordExists')->setMessage('El nombre de la portada "%value%" ya esta registrado');
		
		
		$descripcion = $this->createElement('textarea','descripcion');
		$descripcion ->setLabel('Descripción de la palabra')
		->setAttrib('cols', '70')
		->setAttrib('rows', '8')
		->setRequired(true)
		->addValidator('NotEmpty');
		
		$descripcion ->getValidator('NotEmpty')->setMessage('Este campo no puede estar vacío');
		
		
		$keywords = $this->createElement('text','keywords');
		$keywords->setLabel('Keywords');
		$keywords->setAttrib('size','50');
		
		
		  $file = new Zend_Form_Element_File('image');
        	$file->setLabel('Imagen de portada')
            ->setDestination(getcwd() . '/portadas')
            ->setValueDisabled(true)
            ->setRequired(true)
        	->addErrorMessage('No has seleccionado una imagen')
        	->addValidator('Count', false, 1)
			->addValidator(new Zend_Validate_File_IsImage());
		
		
		$register = $this->createElement('submit','submit');
		$register->setLabel("Guardar")
				->setIgnore(true)->setAttrib('class','submit');
		
		$this->addElements(array($nombre_portada,$file,$descripcion,$keywords,$categorias,$register));
	}
	
}