<?php
class DonativoController extends Zend_Controller_Action
{
	private $hostname;
	private $urlactual;
	private $facebook;
	private $loginurl; 
	private $logouturl;

	
	
	/**
	 * Inicializa el objeto facebook y los parametros de login logout
	 * asi como el hostname actual;
	 */
    public function init()
    {
    	$this->view->doctype('XHTML1_RDFA');
    	$this->urlactual = Zend_Controller_Front::getInstance()->getRequest()->getRequestUri();
    	$request = Zend_Controller_Front::getInstance()->getRequest();
    	$this->hostname=$request->getHttpHost();
    	$this->urlactual="http://".$this->hostname.$this->urlactual;
    	$this->view->urlactual=$this->urlactual;
    	$this->view->hostname=$this->hostname;
    	$this->view->fbappid=Zend_Registry::get('fbappid');
    	$this->view->fbsecret=Zend_Registry::get('fbsecret');
    	
    	require_once 'facebook/facebook.php';
    	$config = array(
    			'appId'=>Zend_Registry::get('fbappid'),
    			'secret'=>Zend_Registry::get('fbsecret'),
                        'cookie' => true
    			);
    	$this->facebook=new Facebook($config);
    	$params = array(
    			'scope' => 'user_about_me,email,publish_stream,publish_actions,user_actions:tercerdia,friends_actions:tercerdia,friends_about_me,user_photos',
    			'redirect_uri' => 'http://'.$this->hostname.$request->getBaseUrl().'/?login=1'
    	);
    	$this->loginurl = $this->facebook->getLoginUrl($params);
    	$params=array('next'=>'http://'.$this->hostname.$request->getBaseUrl().'/?logout=1');
    	$this->logouturl=$this->facebook->getLogoutUrl($params);
    	
    }

    /**
     * Cancelación de un donativo
     */
    public function cancelAction(){
    	$this->_redirect("/");	
    }
    
    /**
     * Gracias por tu donativo
     */
    public function graciasAction(){
    	
    }
    
}

