<?php

class IndexController extends Zend_Controller_Action
{
	private $hostname;
	private $urlactual;
	private $facebook;
	private $loginurl; 
	private $logouturl;

	
	
	/**
	 * Inicializa el objeto facebook y los parametros de login logout
	 * asi como el hostname actual;
	 */
    public function init()
    {
    	$this->view->doctype('XHTML1_RDFA');
    	$this->urlactual = Zend_Controller_Front::getInstance()->getRequest()->getRequestUri();
    	$request = Zend_Controller_Front::getInstance()->getRequest();
    	$this->hostname=$request->getHttpHost();
    	$this->urlactual="http://".$this->hostname.$this->urlactual;
    	$this->view->urlactual=$this->urlactual;
    	$this->view->hostname=$this->hostname;
    	$this->view->fbappid=Zend_Registry::get('fbappid');
    	$this->view->fbsecret=Zend_Registry::get('fbsecret');
    	
    	require_once 'facebook/facebook.php';
    	$config = array(
    			'appId'=>Zend_Registry::get('fbappid'),
    			'secret'=>Zend_Registry::get('fbsecret'),
                        'cookie' => true
    			);
    	$this->facebook=new Facebook($config);
    	$params = array(
    			'scope' => 'user_about_me,email,publish_stream,publish_actions,user_actions:tercerdia,friends_actions:tercerdia,friends_about_me,user_photos',
    			'redirect_uri' => 'http://'.$this->hostname.$request->getBaseUrl().'/?login=1'
    	);
    	$this->loginurl = $this->facebook->getLoginUrl($params);
    	$params=array('next'=>'http://'.$this->hostname.$request->getBaseUrl().'/?logout=1');
    	$this->logouturl=$this->facebook->getLogoutUrl($params);
    	
    }
	
    
    /**
     * Maneja los perfiles de los usuarios
     */
    public function perfilAction(){
          $request = Zend_Controller_Front::getInstance()->getRequest();
    	$this->view->doctype('XHTML1_RDFA');
        
        $auth=Zend_Auth::getInstance();
    	$resultado=$auth->authenticate(new facebook_authfacebook($this->facebook));
    	$userauth=$resultado->getIdentity();
    	$this->view->userauth=$userauth;
        $this->view->user=$this->facebook->getUser();
        if($this->view->user)
                    {$this->view->userinfo=$this->facebook->api('/'.$this->view->user);}
    }
    
    
    
    /**
     * Maneja la lista de lideres
     */
    public function lideresAction(){
        $request = Zend_Controller_Front::getInstance()->getRequest();
    	$this->view->doctype('XHTML1_RDFA');
        
        $auth=Zend_Auth::getInstance();
    	$resultado=$auth->authenticate(new facebook_authfacebook($this->facebook));
    	$userauth=$resultado->getIdentity();
    	$this->view->userauth=$userauth;
        $this->view->user=$this->facebook->getUser();
        if($this->view->user)
                    {$this->view->userinfo=$this->facebook->api('/'.$this->view->user);}
        
        $itemxpagina=30;
        $usuarios=new Application_Model_Usuario();
        $pagina=(int)Zend_Controller_Front::getInstance()->getRequest()->getParam('page',1);
        $lideresxdescarga=$usuarios->listar_pordescargas();
        Zend_View_Helper_PaginationControl::setDefaultViewPartial('partials/control_lider.phtml'); 
        $paginator = Zend_Paginator::factory($lideresxdescarga);
        $paginator->setItemCountPerPage($itemxpagina);
        $paginator->setCurrentPageNumber($pagina);
        $this->view->paginator=$paginator;
        $this->view->lideresxdescarga=$lideresxdescarga;
        $this->view->pos=(($pagina*$itemxpagina)-$itemxpagina+1);
        
        if($pagina>$paginator->getPages()->pageCount){
             $this->_redirect('/lideres/pagina/1');
        }
    }
    /**
     * Maneja la pagina principal del sistema
     * asi como las paginas recorridas mediante el paginator
     */
    public function indexAction(){
    	$request = Zend_Controller_Front::getInstance()->getRequest();
    	$this->view->doctype('XHTML1_RDFA');
    	$logout=(int)Zend_Controller_Front::getInstance()->getRequest()->getParam('logout');
    	if($logout)	{
    		$this->facebook->destroySession();
    		Zend_Auth::getInstance()->clearIdentity();
    		$this->_redirect("http://".$this->hostname.$request->getBaseUrl().'/?'.time());
    	}
    	$this->view->messages = $this->_helper->flashMessenger->getMessages();
    	$auth=Zend_Auth::getInstance();
    	$resultado=$auth->authenticate(new facebook_authfacebook($this->facebook));
    	$userauth=$resultado->getIdentity();
    	$this->view->userauth=$userauth;
    	
    	
    	$session = new Zend_Session_Namespace('urlback');
		$session->urlback=$this->urlactual;
    	$this->view->loginurl=$this->loginurl;
    	$this->view->logouturl=$this->logouturl;
    	$this->view->user=$this->facebook->getUser();
    	if($this->view->user)
                    {$this->view->userinfo=$this->facebook->api('/'.$this->view->user);
                         $dbusuario=new Application_Model_Usuario();
                         $data=array('cFechaActividad' => new Zend_Db_Expr('NOW()'));
	     $dbusuario->update($data,'idUsuario='.$this->view->userauth['id']);
                    }
    	$page = $this->getRequest()->getParam('page', 1);
    	$this->view->page=$page;
    	
    	$cat= $this->getRequest()->getParam('cat',0);
		$this->view->cat=$cat;
                
    	$categorias=new Application_Model_Categoria();
    	$this->view->categorias=$categorias->listar_ordenados();
             
            $this->view->nombreCat='todos';
            foreach ($this->view->categorias as $categoria){
                if($categoria['idCategoria']==$cat){
                    $this->view->nombreCat=$categoria['cTitulo'];
                    break;
                }
            }
                    

    	$backgrounds= new Application_Model_Background();
    	
    	$cache		=	new Application_Model_Cache();
    	if(!$cache->getData("cachebacks".$cat) ){
    		$cache->setData("cachebacks".$cat, $backgrounds->listar_ordenadosxcategoria($cat),60*60*24*3);
    		$backs	=	$cache->getData("cachebacks".$cat);	
    	}else{
    		$backs	=	$cache->getData("cachebacks".$cat);
    	}
    		
      	$this->view->backgrounds= $backs; 	
    	
    	$login=(int)Zend_Controller_Front::getInstance()->getRequest()->getParam('login');
    	if($login){
    		$this->_redirect("http://".$this->hostname.$request->getBaseUrl().'/index/altausuario');
    	}
    	
    	
    }
    
    public function altausuarioAction(){
    	$this->view->doctype('XHTML1_RDFA');
    	$request = Zend_Controller_Front::getInstance()->getRequest();
    	$auth=Zend_Auth::getInstance();
    	$resultado=$auth->authenticate(new facebook_authfacebook($this->facebook));
    	$userauth=$resultado->getIdentity();
    	
    	if(Zend_Auth::getInstance()->hasIdentity()){
    		$identity = Zend_Auth::getInstance()->getIdentity();

		    	$this->view->userauth=$userauth;
		    	$dbusuario=new Application_Model_Usuario();
		    	$usuario=$dbusuario->get($this->view->userauth['id']);
		    	if($this->view->userauth['location'])
		    	{$pais=$this->view->userauth['location']['name'];}
		    	else
		    	{$pais="";}
		    	if(!$usuario){
		    		$data=array(
		    				'idUsuario' => $this->view->userauth['id'],
		    				'cNombre'	=>$this->view->userauth['name'],
		    				'cMail'		=>$this->view->userauth['email'],
		    				'cToken'	=>$this->facebook->getAccessToken(),
		    				'cPais'		=>$pais,
		    				'cFechaActividad'	=>  new Zend_Db_Expr('NOW()')
		    		);
		    		$dbusuario->insert($data);
		    	}
		    	else {
		    		$data=array(
		    				'cFechaActividad' => new Zend_Db_Expr('NOW()')
		    		);
		    		$dbusuario->update($data,'idUsuario='.$this->view->userauth['id']);
		    	}
		    	$this->_redirect("http://".$this->hostname.$request->getBaseUrl().'/?'.time());

    }
    	$this->_redirect("http://".$this->hostname.$request->getBaseUrl());
  }
    
    /**
     * crea la instancia de la pagina de descarga del sistema
     */
    public function descargaAction(){
    	$this->view->doctype('XHTML1_RDFA');
    	$this->_helper->layout->disableLayout();
    	$id=Zend_Controller_Front::getInstance()->getRequest()->getParam('id');
    	$id=(int)$id;
    	$background=new Application_Model_Background();
    	$this->view->image=$background->get($id);
    	$data=array('iDescargas'=>($this->view->image['iDescargas']+1));
    	$background->update($data,'idBackground='.$this->view->image['idBackground']);
                    $this->view->user=$this->facebook->getUser();
    	if($this->view->user)
                    {$this->view->userinfo=$this->facebook->api('/'.$this->view->user);
                         $dbusuario=new Application_Model_Usuario();
                         $data=array('iDescargas' => new Zend_Db_Expr('iDescargas + 1'));
	     $dbusuario->update($data,'idUsuario='.$this->view->user);
                    }
                    $this->_redirect('/download.php?imagen='.basename($this->view->image['cUrl'])) ;
    }
    
    /**
     * Muestra una portada seleccionada
     */
    public function portadaAction(){
    	//portadas/index/uploadportada/id/31   
    	$this->view->doctype('XHTML1_RDFA');
    	$auth=Zend_Auth::getInstance();
    	$resultado=$auth->authenticate(new facebook_authfacebook($this->facebook));
    	$userauth=$resultado->getIdentity();
    	$this->view->userauth=$userauth;
    	$this->view->loginurl=$this->loginurl;
    	$this->view->logouturl=$this->logouturl;
    	$this->view->user=$this->facebook->getUser();
    	$backgrounds= new Application_Model_Background();
    	$id=Zend_Controller_Front::getInstance()->getRequest()->getParam('id');
    	$this->view->nombre=htmlentities(Zend_Controller_Front::getInstance()->getRequest()->getParam('nombre'));
    	$this->view->id=$id;
    	$this->view->background=$backgrounds->get($id);
	    	if($this->view->user){
	    		$this->view->userinfo=$this->facebook->api('/'.$this->view->user);
                                                              $dbusuario=new Application_Model_Usuario();
                                                                $data=array('iVistas' => new Zend_Db_Expr('iVistas + 1'));
                                                                $dbusuario->update($data,'idUsuario='.$this->view->userauth['id']);
	    	}
    	
    	
		$this->view->sicreo=0;    	
    	if($this->view->user){
    		$yocreo=$this->facebook->api('/'.$this->view->user.'/tercerdia:believe?fields=data');
	    	if($yocreo['data']){
	    		foreach($yocreo['data'] as $palabra){
	    			if($palabra['data']['word']['title']==$this->view->background['cTitulo'])
	    				$this->view->sicreo=1;			
	    		}
    		}
    	}
       	$data=array('iVistas'=>($this->view->background['iVistas']+1));
    	$backgrounds->update($data,'idBackground='.$this->view->background['idBackground']);
                    
    }
    
    
    public function uploadportadaAction(){
    	$request = Zend_Controller_Front::getInstance()->getRequest();
    	$id=Zend_Controller_Front::getInstance()->getRequest()->getParam('id',0);
    	$id=(int)$id;
    	$backgrounds= new Application_Model_Background();
    	$bg=$backgrounds->get($id);
    	
    	if(Zend_Auth::getInstance()->hasIdentity() && $bg ){
			    $identity = Zend_Auth::getInstance()->getIdentity();
    			$this->view->userauth=$identity;
    			   			
    			
    			$params= array(
	    			'method'	=>	'fql.query',
	    			'query'		=> 'SELECT aid,object_id, owner, name, object_id FROM album where owner=me() and name="TercerDia.com"'
	    					);
	    		$result=$this->facebook->api($params);
	    		if(!$result){
	    			$album_details = array(
	        			'message'=> 'Portadas para Facebook con sentido, www.tercerdia.com',
	        			'name'=> 'TercerDia.com'
					);
					$create_album = $this->facebook->api('/'.$this->facebook->getUser().'/albums', 'post', $album_details);
					$albumid=$create_album['id'];
	    		}else{
	    			$albumid=$result[0]['object_id'];
	    		}
	    			$fotos=$this->facebook->api('/'.$albumid.'/photos?fields=name,id');
	    			$encontrado=0;
					foreach($fotos["data"] as $foto){
						if(strtoupper($foto['name'])==strtoupper('Portada de facebook - '.$bg['cTitulo'])){
							$encontrado=1;
							$idFacebookfoto=$foto['id'];
							break;
						}
					}
					
					if(!$encontrado){
						$this->facebook->setFileUploadSupport(true);
	    				$args = array('message' => 'Portada de facebook - '.$bg['cTitulo']);
						$args['image'] = '@' . (getcwd().$bg['cUrl']);
						$data = $this->facebook->api('/'. $albumid . '/photos', 'post', $args);
						//$this->facebook->api('/'.$data['post_id'],'delete');
						$idFacebookfoto=$data['id'];
					}
                                        
                                       // Zend_Debug::dump($data);
                                       // Zend_Debug::dump($fotos);
                                       // die;
                                                          $this->view->user=$this->facebook->getUser();
                                                        $dbusuario=new Application_Model_Usuario();
                                                          $data=array('iCargas' => new Zend_Db_Expr('iCargas + 1'));
                                                          $dbusuario->update($data,'idUsuario='.$this->view->user);
                    
					$this->_redirect("http://www.facebook.com/profile.php?preview_cover=".$idFacebookfoto);
	     }
    	else{
    		$this->_redirect($this->loginurl);
    	}
    	
    }
    
    
    /**
     * borrar un background
     */
    public function deleteportadaAction(){
    	$this->view->doctype('XHTML1_RDFA');
    	$request = Zend_Controller_Front::getInstance()->getRequest();
    	if(Zend_Auth::getInstance()->hasIdentity()){
			    $identity = Zend_Auth::getInstance()->getIdentity();
			    if($identity['admin']){
					$this->view->userauth=$identity;
					$idBackground=Zend_Controller_Front::getInstance()->getRequest()->getParam('idBackground');
					$idBackground=(int)$idBackground;
					$session = new Zend_Session_Namespace('urlback');
					$this->view->urlback=$session->urlback;
					$dbBackground = new Application_Model_Background();
					$backgroundimg=$dbBackground->get($idBackground);
					$archivo=getcwd().$backgroundimg['cUrl'];
					if(is_file($archivo)){
						unlink($archivo); 
					}
					$dbRelBackgroundCategoria = new Application_Model_RelBackgroundCategoria();
					$dbRelBackgroundCategoria->delete('idBackground='.$idBackground);
					
					$dbBackground->delete('idBackground='.$idBackground);
					$flashmessenger=$this->_helper->getHelper('FlashMessenger');
					$flashmessenger->addMessage('portadaeliminada');
					$this->_redirect("http://".$this->hostname.$request->getBaseUrl());
				}
    	 		else{
			    	$this->_redirect("http://".$this->hostname.$request->getBaseUrl());
			    }
		   }
	   else{
		   	$this->_redirect("http://".$this->hostname.$request->getBaseUrl());
		   }
    }
    
    public function teamAction(){
         
    }
    
    public function nuevacategoriaAction(){
    	$this->view->doctype('XHTML1_RDFA');
    	$request = Zend_Controller_Front::getInstance()->getRequest();
    	if(Zend_Auth::getInstance()->hasIdentity()){
			    $identity = Zend_Auth::getInstance()->getIdentity();
			    if($identity['admin']){
					$this->view->userauth=$identity;
								    	
			    }
    	 		else{
			    	$this->_redirect("http://".$this->hostname.$request->getBaseUrl());
			    }
		   }
	   else{
		   	$this->_redirect("http://".$this->hostname.$request->getBaseUrl());
		   }
    }
    
    /**
     * para crear una nueva portada 
     */
    public function nuevoAction(){
    	$this->view->doctype('XHTML1_RDFA');
    	$request = Zend_Controller_Front::getInstance()->getRequest();
    	if(Zend_Auth::getInstance()->hasIdentity()){
			    $identity = Zend_Auth::getInstance()->getIdentity();
			    if($identity['admin']){
			    	$this->view->messages = $this->_helper->flashMessenger->getMessages();
			    	$this->view->userauth=$identity;
			    	if(isset($_POST['descripcion']))
			    	$_POST['descripcion']=stripslashes($_POST['descripcion']);
			    	if(isset($_POST['nombre_portada']))
			    	$_POST['nombre_portada']=stripslashes($_POST['nombre_portada']);
			    	$form=new Application_Form_addForm();
			    	$this->view->formulario=$form;
			    	
			    	if($this->getRequest()->isPost()){
			    		if(!$form->isValid($_POST)){
			    			array_map('stripslashes', $_POST);
			    		}
			    		else{
			    			$values = $form->getValues();
			    			$this->view->valores=$values;
			    			$filename=pathinfo($values['image']);
			    			$newfilename=uniqid('img_').'.'.strtolower($filename['extension']);
			    			$this->view->valores['nuevonombre']=$newfilename;
			    			$element = $form->getElement('image');
			    			$element->addFilter('Rename',array('target' => getcwd().'/portadas/'.$newfilename ));
			    			$element->receive();
			    			$dbBackground=new Application_Model_Background();
			    			$data=array('cTitulo'		=>  ($values['nombre_portada']),
			    						'cDescripcion'	=>  ($values['descripcion']),
			    						'cUrl' 			=>	'/portadas/'.$newfilename,
			    						'cFechaHora'	=> new Zend_Db_Expr('NOW()'),
			    						'cKeywords'		=> ($values['keywords'])
			    						);
			    			$idBackground=$dbBackground->insert($data);
			    			$dbCategoria=new Application_Model_RelBackgroundCategoria();

			    			foreach($values['categorias'] as $categoria){
			    				$data=array('idBackground'	=> (int)$idBackground,
			    							 'idCategoria'	=> $categoria);
			    				$dbCategoria->insert($data);
			    			}
			    			
			    				$flashmessenger=$this->_helper->getHelper('FlashMessenger');
			    				$flashmessenger->addMessage('Portada nueva creada exitosamente');
								$form->reset();
								$cache	= new Application_Model_Cache();
								for($i=0;$i<12;$i++){
									$cache->removeData("cachebacks".$i);
								}
								$this->_redirect("http://".$this->hostname.$request->getBaseUrl().'/admin/nuevo');
			    		}
			    	}
			    	
			    }
			    else{
			    	$this->_redirect("http://".$this->hostname.$request->getBaseUrl());
			    }
			    	
		   }
		   else{
			    	$this->_redirect("http://".$this->hostname.$request->getBaseUrl());}
		   
    }


}