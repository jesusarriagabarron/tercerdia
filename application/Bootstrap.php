<?php

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{
	protected function _initSession()
	{
		Zend_Session::start();
		// What goes here!?
		Zend_Registry::set('fbappid', '157379247736751');
		Zend_Registry::set('fbsecret', '0058be6a57ddd1958ff4218abee1b3dc');
		
		$autoloader = Zend_Loader_Autoloader::getInstance();
		$autoloader->registerNamespace('facebook_');

		
		/**
		 * Manejo de caché de datos
		 */
		$frontendOptions = array(
				'lifetime' => 7200,
				'automatic_serialization' => true
		);
		$backendOptions = array(
				'cache_dir' => APPLICATION_PATH.'/../cache'
		);
		$CacheGeneral = Zend_Cache::factory('Core',
				'File',
				$frontendOptions,
				$backendOptions);
		Zend_Registry::set('cache',$CacheGeneral);
		
		
	}
	

}

