<?php
class Application_Model_RelBackgroundCategoria extends Zend_Db_Table_Abstract{
	protected	$_name='TDBackgroundCategoria';

	
	public function getBackgroundCategoria($idBackground){
		$select = $this->_db->select()
				->from('TDBackgroundCategoria',array('idBackground','idCategoria'))
				->joinInner('TDCategoria','TDBackgroundCategoria.idCategoria=TDCategoria.idCategoria',array('cTitulo'))
				->where('TDBackgroundCategoria.idBackground=?',$idBackground);
		$row=$this->getAdapter()->fetchAll($select);
		return $row;
	}
	
}