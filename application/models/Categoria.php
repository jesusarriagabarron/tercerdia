<?php
class Application_Model_Categoria extends Zend_Db_Table_Abstract{
	protected	$_name='TDCategoria';

	public function listar_ordenados(){
		$order='cTitulo';
		$select=$this->select()
				->order($order);
		$rows=$this->fetchAll($select);
		return  $rows->toArray();
	}
	
	
}