<?php
class Application_Model_Background extends Zend_Db_Table_Abstract{
	protected	$_name='TDBackground';
	protected 	$_primary='idBackground';
	
	public function listar_ordenados(){
		$rows=$this->fetchAll();
		return  $rows->toArray();
	}
	
	public function get($id){
		$id=(int)$id;
		$row = $this->fetchRow('idBackground = ' . $id);
		if($row){
			return $row->toArray();	
		}else{
			return null;
		}
		
	}
	
	
	public function listar_ordenadosxcategoria($idCategoria=0){
		$idCategoria=(int)$idCategoria;
		$select = $this->_db->select()->distinct('b.idBackground')
				->from(array('b'=>'TDBackground'),array('b.idBackground','b.cTitulo', 'b.cDescripcion', 
				'b.cUrl', 'b.iDescargas', 'b.iVistas', 'b.cFechaHora','b.cKeywords'))
				->joinInner(array('c'=>'TDBackgroundCategoria'),'c.idBackground=b.idBackground',array())
				->joinInner(array('cat'=>'TDCategoria'),'c.idCategoria=cat.idCategoria',array());
		if($idCategoria>0)	
			$select->where('cat.idCategoria=?',$idCategoria);
            
			$select->order(array(new Zend_Db_Expr("RAND() ASC"))); 
			$rows=$this->getAdapter()->fetchAll($select);
		return  $rows;
	}
	
}