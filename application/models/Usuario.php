<?php
class Application_Model_Usuario extends Zend_Db_Table_Abstract{
	protected	$_name='TDUsuario';
	protected 	$_primary='idUsuario';
	
	public function listar_ordenados(){
		$rows=$this->fetchAll();
		return  $rows->toArray();
	}
	
	public function get($idUsuario){
		$idUsuario=$idUsuario;
		$row = $this->fetchRow('idUsuario = ' . $idUsuario);
		if ($row)
			return $row->toArray();
		else 
			return null;
        }
        
        public function listar_pordescargas(){
		$rows=$this->fetchAll($this->select()->order('iVistas DESC')->order('iCargas DESC')->order('iDescargas DESC'));
		return  $rows->toArray();
	}
        
     
	
}